# Weather nearby
Small project for search and show the weather for the 15 nearest from the map point.

[Install app using expo](https://expo.dev/@abraaobarros/MyOpenWeatherMap?serviceType=classic&distribution=expo-go)
assets are low quality in this distribution type

## QuickStart
```bash
  cd react-native/
  yarn
  yarn start
```

## Demo with extra
![Demo2](./react-native/assets/demo2.mov)
## Demo normal features :
![Demo](./react-native/assets/demo.mov)


## Main decisions thoughts:
Some decisions are not recommended for a production app. But in this scope, it shouldn't be a problem.
### Expo or react-native-cli
For the ease of deployment, share code, and dev stability, Expo was chosen for this test. Expo probably starts to be hard to customize; in this case, it is still possible to eject to react-native standalone when features become complex.

### Redux or Context API
Redux is not recommended for small projects with a low shared state between components. That is the main reason I don't use it.
### React-navigation or built-in:
React-navigation is a lib to handle basic animations between scenes. But it adds unnecessary complexity to the project. So, I build simple navigation to coordinate the scenes and their transitions.

### features
* - Search map center using API
* - List cities nearby
* - Show weather for selected city

Mockup of basic features
![Basic features](./react-native/assets/figma.png)

https://www.figma.com/file/AayfTd5YokN7RqYVOT62aK/Weather-App-Figma?node-id=0%3A1


Main components:
1 - SearchButton
2 - MapView
3 - CityList
4 - CityDetail

Scenes:
 - main - Map
 - list - List of cities near the center of the selected map
 - detail - Show the weather in detail for the selected city

Folders structure:
src
 - components # Pure components to be used inside containers
 - repositories # Models, hooks, and requests
 - containers # Compound views with repositories and components, also responsible for navigation
 - index.js # Entrypoint | Async load


 Next Steps:
 - Remove api key from the repository(critical, but it was not in the scope of the test)