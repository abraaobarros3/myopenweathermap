import React, { useRef } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  FlatList,
  Button,
  SafeAreaView,
  TouchableOpacity,
  Animated,
} from "react-native";
import { useNavigation, useNavigationParams } from "../repositories/navigation";
import search from "../../assets/cloud_no_shadow.png";
import icon from "../../assets/cloud_naked.png";
import { EntWeather } from "../repositories/weather";
import WeatherIcon from "../components/WeatherIcon";

export default function CitiesList({}) {
  const [scene, pushScene] = useNavigation();
  const [{ list: cities }, setParams] = useNavigationParams();

  if (scene !== "list") {
    return null;
  }


  function _renderItem({ item }: { item: EntWeather }) {
    return (
      <TouchableOpacity
        style={styles.row}
        onPress={() => {
          pushScene("detail", { item });
        }}
      >
        <Text style={styles.rowText}>{item.name}</Text>
        <View style={styles.icon}>
          <WeatherIcon name={item.weather[0].main} />
        </View>
      </TouchableOpacity>
    );
  }
  function _renderEmptyList(){
    return <View>
      <Text>There is no result for this region</Text>
    </View>
  }
  return (
    <SafeAreaView style={styles.container}>
      <Image source={search} style={styles.image} />
      <FlatList
        data={cities}
        contentContainerStyle={styles.list}
        renderItem={_renderItem}
        ListEmptyComponent={_renderEmptyList}
        keyExtractor={(_, index) => `${index}`}
      />
      <Text style={styles.text}>weather nearby</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  rowText: {
    fontSize: 22,
    color: "white",
  },
  temp: {},
  tempText:{},
  row: {
    backgroundColor: "#99D0F9",
    marginBottom: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 24,
    alignItems: "center",
    paddingHorizontal: 18,
  },
  text: {
    color: "white",
    fontWeight: "bold",
    marginTop: 12
  },
  list: {
    width: Dimensions.get("window").width,
  },
  icon: { width: 42, height: 42, resizeMode: "contain" },
  image: {
    width: 90,
    marginTop: -45,
    marginBottom: 16,
    height: 100,
    resizeMode: "contain",
  },
  container: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#269AF2",
  },
});
