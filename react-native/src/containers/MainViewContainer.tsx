import React, { useState } from "react";
import {
  Button,
  Image,
  View,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  Text,
  TouchableOpacity,
} from "react-native";
import MapView, { Callout, Marker } from "react-native-maps";
import SearchButton from "../components/SearchButton";
import { useNavigation, useNavigationParams } from "../repositories/navigation";
import useWeather, { EntWeather } from "../repositories/weather";
import LottieView from "lottie-react-native";
import WeatherIcon from "../components/WeatherIcon";
import WeatherMarker from "../components/WeatherMarker";


type RegionType = {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
};

const INITIAL_REGION = {
  latitude: 37.78825,
  longitude: -122.4324,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};

let debounceTimeout:NodeJS.Timeout;

export default function MainViewContainer({}) {
  const [scene, pushScene] = useNavigation();
  const [fetchWeather, isLoading] = useWeather();
  const [region, setRegion] = useState(INITIAL_REGION);
  const [markers, setMarkers] = useState<[EntWeather]>();

  const _debouncedSearch = (region: RegionType) => {
      clearTimeout(debounceTimeout);
      debounceTimeout = setTimeout(() => {
        setRegion(region)
        fetchWeather({
          lat: region.latitude,
          lng: region.longitude,
          onCompleted(data) {
            setMarkers(data.list);
          },
        });
      }, 500);
    }

  return (
    <View style={styles.container}>
      <MapView
        minZoomLevel={4} // default => 0
        maxZoomLevel={11}
        style={styles.map}
        onRegionChange={() => {
          // Change back to map if drag the map
          pushScene("main");
        }}

        onRegionChangeComplete={_debouncedSearch}
        onPress={() => {
          // Stay in map when click on map from any scene
          pushScene("main");
        }}
      >
        {markers?.map((item, index) => (
          <WeatherMarker
            item={item}
            onPress={() => {
              pushScene("detail", { item });
            }}
          />
        ))}
      </MapView>

      {scene === "main" && (
        <View style={styles.searchContainer}>
          <View style={styles.searchButton}>
            <SearchButton
              pending={isLoading}
              onPress={() => {
                pushScene("list", { list: markers });
              }}
            />
          </View>
          <Text style={styles.text}>
            weather nearby ({Math.ceil(region.latitude)},{" "}
            {Math.ceil(region.longitude)})
          </Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    backgroundColor: "#269AF2",
  },
  button: {
    margin: 16,
    marginBottom: 32,
    backgroundColor: "white",
  },
  map: {
    position: "absolute",

    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  title: {
    fontWeight: "bold",
    fontSize: 22,
  },
  text: {
    color: "white",
    fontWeight: "bold",
  },
  searchButton: {
    marginTop: -40,
  },
  searchImage: { width: 90, height: 100, resizeMode: "contain" },
  searchContainer: {
    paddingBottom: 32,
    backgroundColor: "#269AF2",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  temp: {
    fontStyle: "italic",
    marginTop: 4,
  },
  plainView: {},
});
