import React from "react";
import { Button, View, Text, Image, StyleSheet } from "react-native";
import { useNavigation, useNavigationParams } from "../repositories/navigation";
import search from "../../assets/cloud_no_shadow.png";
import WeatherIcon from "../components/WeatherIcon";

export default function CityDetail({}) {
  const [scene, pushScene] = useNavigation();
  const [{ item }, setParams] = useNavigationParams();

  if (scene !== "detail" || item === undefined) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Image source={search} style={styles.image} />
      <View style={{height: 200, width: 200}}>
        <WeatherIcon name={item.weather[0].main} />
      </View>
      <Text style={styles.h1}>{item.name}</Text>
      <Text style={styles.h2}>{item.weather[0].description}</Text>
      <View style={styles.temp_container}>
        <Text style={styles.huge}>
          <Text style={styles.small}>max: </Text>
          {Math.floor(item.main.temp_max)} &deg;F
        </Text>
        <Text style={styles.huge}>
          <Text style={styles.small}>min: </Text>
          {Math.floor(item.main.temp_min)} &deg;F
        </Text>
      </View>
      <Button
        title="back"
        color="white"
        onPress={() => {
          pushScene("list");
        }}
      ></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 90,
    marginTop: -45,
    marginBottom: 16,
    height: 100,
    resizeMode: "contain",
  },
  temp_container: {
    padding: 44,
  },
  h1: {
    color: "white",
    fontSize: 36,
    fontWeight: "bold",
    margin: 16,
    textTransform: "uppercase",
  },
  h2: {
    color: "#93CDF8",
    textTransform: "capitalize",
    fontSize: 24,
  },
  huge: {
    color: "white",
    fontSize: 72,
  },
  small: {
    color: "white",
    fontSize: 18,
  },
  text: {},
  container: {
    flex: 5,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#269AF2",
  },
});
