import React from 'react';

type Props = {
  name: string
}
import LottieView from "lottie-react-native";

const toIcon = {
  Clear: require("../../assets/64906-sunny.json"),
  Clouds: require("../../assets/64956-cloudy-icon.json"),
  Snow: require("../../assets/64966-snow-icon.json"),
  Rain: require("../../assets/64960-rainy-icon.json"),
  Drizzle: require("../../assets/64960-rainy-icon.json"),
  Thunderstorm: require("../../assets/64960-rainy-icon.json"),
};

export default function WeatherIcon({name}:Props){
  //openweathermap.org/img/wn/10d@2x.png
  return <LottieView autoPlay source={toIcon[name]}/>
}