import React from "react";
import { Dimensions, View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Callout, Marker } from "react-native-maps";
import { EntWeather } from "../repositories/weather";
import WeatherIcon from "./WeatherIcon";

type Props = {
  item: EntWeather;
  onPress?():void;
};

export default function WeatherMarker({ item, onPress }: Props) {
  return (
    <Marker

      coordinate={{
        latitude: item.coord.lat,
        longitude: item.coord.lon,
      }}
      title={item.name}
      description={`${item.weather[0].description}`}
      image={{
        uri: `http://openweathermap.org/img/wn/${item.weather[0].icon}@4x.png`,
      }}
    >
      <Callout style={styles.plainView}>
        <TouchableOpacity onPress={onPress}>
          <View>
            <View style={styles.container}>
              <Text style={styles.title}>{item.name}</Text>
              <View style={{ width: 30, height: 30 }}>
                <WeatherIcon name={item.weather[0].main} />
              </View>
            </View>
            <Text>{item.weather[0].description}</Text>
            <Text style={styles.temp}>
              ({Math.floor(item.main.temp_min)}-{" "}
              {Math.floor(item.main.temp_max)}) &deg;F
            </Text>
          </View>
        </TouchableOpacity>
      </Callout>
    </Marker>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },
  title: {
    fontWeight: "bold",
    fontSize: 22,
  },
  text: {
    color: "white",
    fontWeight: "bold",
  },
  temp: {
    fontStyle: "italic",
    marginTop: 4,
  },
  plainView: {},
});
