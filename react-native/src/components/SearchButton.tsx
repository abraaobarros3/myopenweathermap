import React from "react";
import {
  ActivityIndicator,
  Button,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import search from "../../assets/search_button.png";
import loading from "../../assets/loading.png";

type Props = {
  onPress(): void;
  pending?: boolean;
};

export default function ({ onPress, pending }: Props): JSX.Element {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
      {pending ? (
        <View>
          <Image source={loading} style={styles.searchImage} />
          <ActivityIndicator
            style={styles.indicator}
            size="large"
            color="#269AF2"
          />
        </View>
      ) : (
        <View>
          <Image source={search} style={styles.searchImage} />
        </View>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  indicator: { position: "absolute", top: 28, left: 28 },
  searchImage: { width: 90, height: 100, resizeMode: "contain" },
});
