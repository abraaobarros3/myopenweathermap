import { useState, useCallback } from "react";

const API_KEY = "6ce48333cc3a1215416840d35006812c";

const BASE_URL = (lat: number, lng: number) =>
  `http://api.openweathermap.org/data/2.5/find?lat=${lat}&lon=${lng}&cnt=15&APPID=${API_KEY}`;

type WeatherResponse = {
  list: [EntWeather]
}

export type EntWeather = {
  name: string;
  coord:{
    lat: number,
    lon: number
  }
  main: {
    temp_max: number;
    temp_min: number;
  };
  weather: [
    {
      main:string;
      description: string;
    }
  ];
};

type Props = {
  lat: number;
  lng: number;
  onCompleted(data: WeatherResponse): void;
};

export default function useWeather(): [(payload: Props) => void, boolean] {
  const [isLoading, setiIsLoading] = useState<boolean>(false);
  const fetchWeatherFromCenter = useCallback(
    async ({ lat, lng, onCompleted }: Props) => {
      setiIsLoading(true);

      const response = await fetch(BASE_URL(lat, lng));
      const payload = await response.json() as WeatherResponse
      onCompleted(payload);
      setiIsLoading(false);
    },
    []
  );

  return [fetchWeatherFromCenter, isLoading];
}
