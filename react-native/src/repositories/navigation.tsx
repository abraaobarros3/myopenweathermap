import React, { useContext, createContext, useState } from "react";
import { View } from "react-native";
import { EntWeather } from "./weather";

type NavigationParams = {
  list?: [EntWeather];
  item?: EntWeather;
};

type NavigationProps = {
  scene: string;
  params: NavigationParams;
  push(scene: string, params?: NavigationParams): void;
  setParams(state: {}): void;
};

const NavigationContext = createContext<NavigationProps>({
  scene: "main",
  params: {},
  push: () => {},
  setParams: ({}) => {},
});

export function useNavigation(): [
  string,
  (scene: string, params?: NavigationParams) => void
] {
  const { scene, push } = useContext(NavigationContext);
  return [scene, push];
}

export function useNavigationParams(): [
  NavigationParams,
  (params: NavigationParams) => void
] {
  const { params, setParams } = useContext(NavigationContext);
  return [params, setParams];
}

type RouterProps = {
  children?: JSX.Element;
  routes: { [name: string]: JSX.Element };
};

export function Router({ routes }: RouterProps) {
  const [scene, setScene] = useState<string>("main");
  const [params, setParams] = useState<{}>({});
  return (
    <NavigationContext.Provider
      value={{
        scene: scene,
        push: (scene, params) => {
          setScene(scene);
          params && setParams((prev) => ({ ...prev, ...params }));
        },
        params,
        setParams,
      }}
    >
      {Object.keys(routes).map((scene, index) => (
        routes[scene]
      ))}
    </NavigationContext.Provider>
  );
}
