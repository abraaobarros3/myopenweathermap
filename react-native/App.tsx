import React, { useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Button,
  SafeAreaView,
} from "react-native";
import MapView from "react-native-maps";
import CitiesListContainer from "./src/containers/CitiesListContainer";
import CityDetailContainer from "./src/containers/CityDetailContainer";
import MainViewContainer from "./src/containers/MainViewContainer";
import { Router } from "./src/repositories/navigation";
import useWeather from "./src/repositories/weather";
import { Asset } from "expo-asset";

export default function App() {
  useEffect(() => {
    const images = [
      require("./assets/cloud.png"),
      require("./assets/cloud_naked.png"),
      require("./assets/cloud_no_shadow.png"),
      require("./assets/search_button.png"),
      require("./assets/loading.png"),
    ];
    images.map(async (image) => {
      await Asset.fromModule(image).downloadAsync();
    });
  }, []);

  return (
    <Router
      routes={{
        main: <MainViewContainer />,
        list: <CitiesListContainer />,
        detail: <CityDetailContainer />,
      }}
    ></Router>
  );
}
